import { Injectable } from '@angular/core';
import { MoveRequestDto } from '../pages/game/models/dto/move-request';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MoveResponseDto } from '../pages/game/models/dto/move-response';

@Injectable({
  providedIn: 'root'
})

export class MoveService {
  private url = 'https://m63dhb53yf.execute-api.us-east-2.amazonaws.com/dev/moves';

  constructor(private http: HttpClient) { }

  postMove(move: MoveRequestDto): Observable<MoveResponseDto> {
    return this.http.post<MoveResponseDto>(this.url, move, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }).pipe(
      catchError(this.handleError));
  }

  private handleError(err: any) {
    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occurred: ${err.error.message}`;
    }
    console.error(err);
    return throwError(errorMessage);
  }
}
