import { Injectable } from '@angular/core';
import { User } from '../pages/game/models/user';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private url = 'https://m63dhb53yf.execute-api.us-east-2.amazonaws.com/dev/user';

  constructor(private http: HttpClient) { }

  public getUser(name: string): Observable<User> {
    return this.http.get<User>(`${this.url}/${name}`);
  }

}
