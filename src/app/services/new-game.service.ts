import { Injectable } from '@angular/core';
import { NewGameRequestDto } from '../pages/game/models/dto/new-game-request';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MoveResponseDto } from '../pages/game/models/dto/move-response';

@Injectable({
  providedIn: 'root'
})
export class NewGameService {
  private url = 'https://m63dhb53yf.execute-api.us-east-2.amazonaws.com/dev/games';

  constructor(private http: HttpClient) { }

  createNewGame(request: NewGameRequestDto): Observable<MoveResponseDto> {
    return this.http.post<MoveResponseDto>(this.url, request, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    });
  }
}
