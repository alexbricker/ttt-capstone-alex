import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { MoveService } from './move-service.service';
import { MoveRequestDto } from '../pages/game/models/dto/move-request';

describe('MoveServiceService', () => {
  let service: MoveService;
  let httpTestingController: HttpClientTestingModule;
  const gameIdRequest = '1234';
  const moveRequest: MoveRequestDto = {
    gameId: gameIdRequest,
    userId: 'user1',
    move: 0
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        MoveService,
      ]
    });

    service = TestBed.get(MoveService);
    httpTestingController = TestBed.get(HttpTestingController);

  });
});
