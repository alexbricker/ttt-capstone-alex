import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { UserService } from './user.service';
import { UserRequestDto } from '../pages/game/models/dto/user-request';

describe('UserService', () => {
  let service: UserService;
  let httpTestingController: HttpClientTestingModule;
  const userName = 'user name';

  const userRequest: UserRequestDto = {
    userName: ('{userName}')
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        UserService,
      ]
    });
    service = TestBed.get(UserService);
    httpTestingController = TestBed.get(HttpTestingController);

  });

  it('should be created', () => {
    const newService: UserService = TestBed.get(UserService);
    expect(newService).toBeTruthy();
  });

  it('should return User', () => {
    const newService: UserService = TestBed.get(UserService);
    const getUserSpy = spyOn(newService, 'getUser');
    newService.getUser(userName);

    expect(getUserSpy).toHaveBeenCalledWith(userName);
  });
});
