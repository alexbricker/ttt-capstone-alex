import { TestBed, inject } from '@angular/core/testing';

import { NewGameService } from './new-game.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NewGameRequestDto } from '../pages/game/models/dto/new-game-request';

describe('NewGameService', () => {
  const httpTestingController: HttpClientTestingModule = new HttpClientTestingModule();
  const gameRequest: NewGameRequestDto = {
    gameName: 'new game',
    userOneId: '1111',
    userTwoId: '2222',
    tokenOne: 'X',
    tokenTwo: 'O'
  };
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule
    ]
  }));

  it('should be created', () => {
    const service: NewGameService = TestBed.get(NewGameService);
    expect(service).toBeTruthy();
  });
});
