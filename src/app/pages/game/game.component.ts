import { Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { MoveService } from '../../services/move-service.service';
import { MoveRequestDto } from './models/dto/move-request';
import { UserService } from '../../services/user.service';
import { MoveResponseDto } from './models/dto/move-response';
import { MoveDto } from './models/dto/move';
import { NewGameService } from '../../services/new-game.service';
import { NewGameRequestDto } from './models/dto/new-game-request';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})



export class GameComponent {
  private gameInputName = 'generic name';
  private userOneId = '5b298e8b-db2b-440a-b21a-363a8809ebb6';
  private currentUser;
  private human = '78fe488b-35e0-4681-a204-07783b744cb8';
  private computer = '490DB457-4C49-452E-AD27-5C354385A158';
  userOne = true;
  error = false;
  private errorMessage = '';
  gameState = false;
  private winner;
  private state = '';
  private gameId;
  private gameRequest: NewGameRequestDto = {
    gameName: this.gameInputName,
    userOneId: this.userOneId,
    userTwoId: this.human,
    tokenOne: 'X',
    tokenTwo: 'O'
  };
  private move: MoveRequestDto;
  private moveResponse: MoveResponseDto;
  nameInput = true;
  gameSelection = true;
  gameBoard = false;

  private AA = '_';
  private AB = '_';
  private AC = '_';
  private BA = '_';
  private BB = '_';
  private BC = '_';
  private CA = '_';
  private CB = '_';
  private CC = '_';

  constructor(
              private moveService: MoveService,
              private userService: UserService,
              private newGameService: NewGameService) { }

  public newGame(): void {
    window.location.reload();
  }

  public hideSelection(): void {
    this.gameSelection = !this.gameSelection;
    this.gameBoard = !this.gameBoard;
  }

  public startGame(player: string): any {
    this.gameInputName = 'generic';
    this.gameRequest.gameName = 'generic';
    this.gameRequest.userTwoId = player;
    this.newGameService.createNewGame(this.gameRequest).pipe(
      map((moveResponse: MoveResponseDto) => {
        this.moveResponse = {
          gameId: moveResponse.gameId,
          moves: moveResponse.moves,
          nextUserId: moveResponse.nextUserId,
          gameState: moveResponse.gameState
        };
        this.gameId = moveResponse.gameId;
        this.currentUser = moveResponse.nextUserId;
      })
    ).subscribe();
  }

  public async placeMove(moveChoice: number): Promise<any> {
    if(this.gameState){
      return this.state = "Stop doing that. The game is over.";
    }
    this.move = {
      gameId: this.gameId,
      userId: this.currentUser,
      move: moveChoice
    };
    try {
      this.moveResponse = await this.moveService.postMove(this.move).toPromise();
    } catch (error) {
      error = true;
      this.errorMessage = error;
    }
    this.currentUser = this.moveResponse.nextUserId;
    this.displayMoves(this.moveResponse.moves);
    this.checkGameState(this.moveResponse.gameState);
  }

  public checkGameState(stateCheck: number){
    if (this.moveResponse.gameState === 1 ) {
      this.gameState = true;
      if(this.currentUser === this.userOneId){
        this.winner = "O";
      } else {
        this.winner = "X";
      }
      this.state = `${this.winner} has Won`;
    } else if (this.moveResponse.gameState === 2) {
      this.gameState = true;
      this.state = 'Tie';
    }
  }

  public displayMoves(moves: MoveDto[]): void {
    moves.forEach(move => {
      switch (String(move.location)) {
        case '0':
          this.AA = move.token;
          break;
        case '1':
          this.AB = move.token;
          break;
        case '2':
          this.AC = move.token;
          break;
        case '3':
          this.BA = move.token;
          break;
        case '4':
          this.BB = move.token;
          break;
        case '5':
          this.BC = move.token;
          break;
        case '6':
          this.CA = move.token;
          break;
        case '7':
          this.CB = move.token;
          break;
        case '8':
          this.CC = move.token;
          break;
        default:
          break;
      }
    });
  }
}
