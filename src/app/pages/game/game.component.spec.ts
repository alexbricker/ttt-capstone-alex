import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameComponent } from './game.component';
import { MoveService } from '../../services/move-service.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';


describe('GameComponent', () => {
  let component: GameComponent;
  let fixture: ComponentFixture<GameComponent>;
  const httpTestingController: HttpClientTestingModule = new HttpClientTestingModule();
  let postMoveSpy: jasmine.Spy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameComponent ],
      providers: [
        {
          provide: MoveService,
          useValue: jasmine.createSpyObj('MoveService', ['postMove']),
        }
      ],
      imports: [
        HttpClientTestingModule,
      ]
    })
    .compileComponents().then(() => {
      postMoveSpy = TestBed.get(MoveService).postMove as jasmine.Spy;
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('clicking start PvC game', () => {
    it('should call startGame with this.computer', () => {
      const startGameSpy = spyOn(component, 'startGame');
      const PvC = (): HTMLScriptElement => fixture.nativeElement.querySelector('[class="pvc"]');
      const hideGameNameSpy = spyOn(component, 'hideSelection');

      PvC().click();
      fixture.detectChanges();

      expect(startGameSpy).toHaveBeenCalledWith('490DB457-4C49-452E-AD27-5C354385A158');
      expect(hideGameNameSpy).toHaveBeenCalled();
    });
  });

  describe('clicking start PvP game', () => {
    it('should call startGame with this.human', () => {
      const startGameSpy = spyOn(component, 'startGame');
      const PvP = (): HTMLScriptElement => fixture.nativeElement.querySelector('[class="pvp"]');
      const hideGameNameSpy = spyOn(component, 'hideSelection');

      PvP().click();
      fixture.detectChanges();

      expect(startGameSpy).toHaveBeenCalledWith('78fe488b-35e0-4681-a204-07783b744cb8');
      expect(hideGameNameSpy).toHaveBeenCalled();
    });
  });

  describe('on grid click', () => {

    it('should send move to move-service', () => {
      component.placeMove(0);
      expect(postMoveSpy).toHaveBeenCalled();
    });
    it('should call function placeMove with grid reference AA', () => {
      const gridSpace = (): HTMLScriptElement => fixture.nativeElement.querySelector('[class="AA"]');
      const placeMoveSpy = spyOn(component, 'placeMove');

      gridSpace().click();
      fixture.detectChanges();

      expect(placeMoveSpy).toHaveBeenCalledWith(0);
    });
    it('should call function placeMove with grid reference AB', () => {
      const gridSpace = (): HTMLScriptElement => fixture.nativeElement.querySelector('[class="AB"]');
      const placeMoveSpy = spyOn(component, 'placeMove');

      gridSpace().click();
      fixture.detectChanges();

      expect(placeMoveSpy).toHaveBeenCalledWith(1);
    });
    it('should call function placeMove with grid reference AC', () => {
      const gridSpace = (): HTMLScriptElement => fixture.nativeElement.querySelector('[class="AC"]');
      const placeMoveSpy = spyOn(component, 'placeMove');

      gridSpace().click();
      fixture.detectChanges();

      expect(placeMoveSpy).toHaveBeenCalledWith(2);
    });
    it('should call function placeMove with grid reference BA', () => {
      const gridSpace = (): HTMLScriptElement => fixture.nativeElement.querySelector('[class="BA"]');
      const placeMoveSpy = spyOn(component, 'placeMove');

      gridSpace().click();
      fixture.detectChanges();

      expect(placeMoveSpy).toHaveBeenCalledWith(3);
    });
    it('should call function placeMove with grid reference BB', () => {
      const gridSpace = (): HTMLScriptElement => fixture.nativeElement.querySelector('[class="BB"]');
      const placeMoveSpy = spyOn(component, 'placeMove');

      gridSpace().click();
      fixture.detectChanges();

      expect(placeMoveSpy).toHaveBeenCalledWith(4);
    });
    it('should call function placeMove with grid reference BC', () => {
      const gridSpace = (): HTMLScriptElement => fixture.nativeElement.querySelector('[class="BC"]');
      const placeMoveSpy = spyOn(component, 'placeMove');

      gridSpace().click();
      fixture.detectChanges();

      expect(placeMoveSpy).toHaveBeenCalledWith(5);
    });
    it('should call function placeMove with grid reference CA', () => {
      const gridSpace = (): HTMLScriptElement => fixture.nativeElement.querySelector('[class="CA"]');
      const placeMoveSpy = spyOn(component, 'placeMove');

      gridSpace().click();
      fixture.detectChanges();

      expect(placeMoveSpy).toHaveBeenCalledWith(6);
    });
    it('should call function placeMove with grid reference CB', () => {
      const gridSpace = (): HTMLScriptElement => fixture.nativeElement.querySelector('[class="CB"]');
      const placeMoveSpy = spyOn(component, 'placeMove');

      gridSpace().click();
      fixture.detectChanges();

      expect(placeMoveSpy).toHaveBeenCalledWith(7);
    });
    it('should call function placeMove with grid reference CC', () => {
      const gridSpace = (): HTMLScriptElement => fixture.nativeElement.querySelector('[class="CC"]');
      const placeMoveSpy = spyOn(component, 'placeMove');

      gridSpace().click();
      fixture.detectChanges();

      expect(placeMoveSpy).toHaveBeenCalledWith(8);
    });
  });
});
