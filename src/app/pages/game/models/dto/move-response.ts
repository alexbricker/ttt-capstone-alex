import { MoveDto } from './move';

export class MoveResponseDto {
    gameId: string;
    moves: MoveDto[];
    nextUserId: string;
    gameState: number;
}
