export class NewGameRequestDto {
    gameName: string;
    userOneId: string;
    userTwoId: string;
    tokenOne: string;
    tokenTwo: string;
}
