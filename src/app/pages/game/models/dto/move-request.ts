export class MoveRequestDto {
    gameId: string;
    userId: string;
    move: number;
}
