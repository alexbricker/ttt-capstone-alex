import { GameMoves } from './game-moves';

export class UserGame {
    userGameId: string;
    gameId: string;
    userId: string;
    token: string;
    userOne: boolean;
    gameMoves: GameMoves[];
}
