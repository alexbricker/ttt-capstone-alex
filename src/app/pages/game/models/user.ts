import { UserGame } from './user-game';

export class User {
    userId: string;
    userName: string;
    userGames: UserGame[];
}
