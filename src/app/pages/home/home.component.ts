import { Component, OnInit } from '@angular/core';
import { NewGameService } from '../../services/new-game.service';
import { NewGameRequestDto } from '../game/models/dto/new-game-request';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
              private newGameService: NewGameService) { }

  ngOnInit() {
  }

}
